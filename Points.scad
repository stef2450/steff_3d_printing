$fn=50;
point=0.0001;
difference(){
hull(){
    cube([10,10,10]);
    translate([5,5,15]) cube(point);
}
rotate([0,90,0]) translate([-5,5,-1]) cylinder(d=2,h=12);
rotate([90,0,0]) translate([5,5,-11]) cylinder(d=2,h=12);
}
