//Planet=[ [0=Size], [1=Position], [2=Color]];
mercury=[ 4.9, [140,10,85], [1, 0.478, 0.039]];
venus=[ 12, [140,10,85], [1, 0.478, 0.039]];
earth=[ 12.8, [140,10,85], [1, 0.478, 0.039]];
mars=[ 6.8, [140,10,85], [1, 0.478, 0.039]];
jupiter=[ 143, [140,10,85], [1, 0.478, 0.039]];
saturn=[ 120.5, [140,10,85], [1, 0.478, 0.039]];
uranus=[ 51.1, [140,10,85], [1, 0.478, 0.039]];
neptune=[ 49.5, [140,10,85], [1, 0.478, 0.039]];
$fn=64;
color([0, 0, 0])cube([600,20,5]);
//Mercury
color([0.7, 0.7, 0.7])translate([5,10,20]) sphere(d=4.9);
color([0.5, 0.5, 0.5])translate([5,10,0])cylinder(r=2,h=20);
//Venus
color([1, 0.356, 0.039])translate([20,10,20]) sphere(d=12);
color([0.5, 0.5, 0.5])translate([20,10,0]) cylinder(r=2,h=20);
//Earth
color([0,1,0]) translate([40,10,20]) sphere(d=12.8);
color([0.5, 0.5, 0.5])translate([40,10,0]) cylinder(r=2,h=20);
//Mars
color(mars[2]) translate([60,10,20]) sphere(d=6.8);
color([0.5, 0.5, 0.5])translate([60,10,0]) cylinder(r=2,h=20);
//Jupiter
color([0.898, 0.850, 0.4])translate([140,10,85]) sphere(d=143);
color([0.5, 0.5, 0.5])translate([140,10,0]) cylinder(r=2,h=20);
//Saturn
difference(){
color([0.996, 0.937, 0.403])translate([330,10,75])rotate([0,-20,0]) cylinder(d=saturn[0]*1.825,h=1);
color([0.996, 0.937, 0.403])translate([330,10,74])rotate([0,-20,0]) cylinder(d=saturn[0]*1.25,h=3);
}
color([0.996, 0.937, 0.403])translate([330,10,75]) sphere(d=saturn[0]);
color([0.5, 0.5, 0.5])translate([330,10,0]) cylinder(r=2,h=20);
//Uranus
difference(){
color([1, 1, 1])translate([480,10,40]) cylinder(d=uranus[0]*1.825,h=1);
color([1, 1, 1])translate([480,10,39]) cylinder(d=uranus[0]*1.8,h=3);
}
color([0.403, 0.996, 0.937])translate([480,10,40]) sphere(d=51.1);
color([0.5, 0.5, 0.5])translate([480,10,0]) cylinder(r=2,h=20);
//Neptun
color([0.019, 0.329, 0.941])translate([560,10,40]) sphere(d=49.5);
color([0.5, 0.5, 0.5])translate([560,10,0]) cylinder(r=2,h=20);
