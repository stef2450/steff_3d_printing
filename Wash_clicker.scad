$fn=64;
$length=155; //small=50.7 / big=155
$height=15;
difference(){
union(){
translate([-$length/2,0,0])cylinder(d=6,h=$height,center=true);
translate([$length/2,0,0])cylinder(d=6,h=$height,center=true);
translate([0,-2,0])cube([$length,2,$height],center=true);
}
translate([-$length/2,0,0])cylinder(d=4,h=$height,center=true);
translate([$length/2,0,0])cylinder(d=4,h=$height,center=true);
translate([0,2,0])cube([$length-2,6,$height],center=true);
}