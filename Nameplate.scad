$fn=64;
X=45;
Y=20;
TextSize=7;
Text2="& Claire";    //Input for your text
Text1="Quentin";    //Input for your text
difference(){
cube([X,Y,2]);
translate([3,Y/2,0])cylinder(d=3,h=2);
}
translate([2,1,0])union(){
translate([X/2,Y/2+TextSize/10,1])linear_extrude(height = 1.5) {
text(str(Text1),font="Liberation Sans:style=Bold", size = TextSize,halign="center");
}
   translate([X/2,Y/2-TextSize-TextSize/10,1])linear_extrude(height = 1.5) {
text(str(Text2),font="Liberation Sans:style=Bold", size = TextSize,halign="center");
}
}