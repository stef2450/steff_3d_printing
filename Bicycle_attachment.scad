$fn=64;
difference(){
    cylinder(d=8,h=4);
    cylinder(d=5,h=4);
    #translate([-5/3,-10,0])cube([5/1.5,10,6]);
}
Ring=[22,26,8]; //Small
//Ring=[48.5,54,12];  //Big
translate([0,Ring[1]/2+5/2,0])difference(){
    cylinder(d=Ring[1],h=Ring[2]);
    cylinder(d=Ring[0],h=Ring[2]);
   #translate([-Ring[0]/4,0,0])cube([Ring[0]/2,Ring[1]/2,Ring[2]]);
}