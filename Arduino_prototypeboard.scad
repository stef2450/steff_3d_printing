$fn=64;

logo="/home/stefan/Downloads/bulles-small_black.png"; 


difference(){
hull(){
translate([10,10,0])cylinder(d=20,h=3);
translate([110,10,0])cylinder(d=20,h=3);
translate([10,72.7,0])cylinder(d=20,h=3);
translate([110,72.7,0])cylinder(d=20,h=3);
}
translate([110,25,2])mirror([1,0,0])scale([0.4,0.4,0.1])surface(file=logo,invert=true); 
translate([11.5,7,-1])cylinder(d=3,h=5);
translate([40,7,-1])cylinder(d=3,h=5);
translate([6.5,58.5,-1])cylinder(d=3,h=5);
translate([53.5,57,-1])cylinder(d=3,h=5);
}
translate([5,5,0])sphere(d=5);
translate([115,5,0])sphere(d=5);
translate([115,77.7,0])sphere(d=5);
translate([5,77.7,0])sphere(d=5);