$scale=2;
difference(){
union(){
linear_extrude(height = 50*$scale, twist = 180/2   , slices = 90*$scale) {
   difference() {
     offset(r = 10*$scale) {
      square(15*$scale, center = true);
     }
     offset(r = 8*$scale) {
       square(15*$scale, center = true);
     }
   }
 }
 for(i = [-25.5:6:25]){   
translate([-31,i,0])cube([62,3,2]);
}
translate([-25,-31.5,0])cube([50,3,2]);
translate([-25,28.5,0])cube([50,3,2]);
for(i = [-25.5:6:25]){   
translate([i,-31,0])cube([3,62,2]);
}
translate([-31.5,-25,0])cube([3,50,2]);
translate([28.5,-25,0])cube([3,50,2]);
}
for(i = [-18.5:6:28]){   
translate([-37,i,0])cube([75,1,1]);
}
for(i = [-18.5:6:28]){   
translate([i,-37,0])cube([1,75,1]);
}
}