$fn = 100;
batteryDiameter = 18;
holderHeight = 15;
doubleHeight = 65+6;
difference() {
    union(){
        cylinder(h=holderHeight, d=batteryDiameter+2.5);
        translate([batteryDiameter+1+ 0.6 + 0.2, 0, 0]) cylinder(h=holderHeight, d=batteryDiameter+2.5);
        }
    translate([0, 0, 1.5]) cylinder(h=holderHeight, d=batteryDiameter+0.5);
    translate([batteryDiameter +1+ 0.6 + 0.2, 0, 1.5]) cylinder(h=holderHeight, d=batteryDiameter+0.5);
    translate([0, 0, 0]) cylinder(h=holderHeight, d=1);
    translate([batteryDiameter + 0.6 + 0.2, 0, 0]) cylinder(h=holderHeight, d=1);
    translate([-batteryDiameter/2-4/2,-batteryDiameter/2-2.5,1.5])cube([batteryDiameter*2+4+5,7,holderHeight]);
translate([-batteryDiameter/2-4/2,-batteryDiameter/2-5,1.5])cube([batteryDiameter*2+4+5,batteryDiameter,1]);
}
translate([0,0,1.3])union(){
difference(){
    translate([-3/2,-20/2,0])cube([3,20,2]);
    translate([-3/2,-20.5/2,0])rotate([8,0,0])cube([4,10.5,3]);
    rotate([0,0,180])translate([-3/2,-20.5/2,0])rotate([8,0,0])cube([4,10.5,3]);
translate([0,0,2])rotate([103,0,0])cylinder(d=1,h=11);
    translate([0,0,2])rotate([-103,0,0])cylinder(d=1,h=11);
    cylinder(d=1,h=2);
    }
}
translate([batteryDiameter+0.8,0,1.3])union(){
difference(){
    translate([-3/2,-20/2,0])cube([3,20,2]);
    translate([-3/2,-20.5/2,0])rotate([8,0,0])cube([4,10.5,3]);
    rotate([0,0,180])translate([-3/2,-20.5/2,0])rotate([8,0,0])cube([4,10.5,3]);
translate([0,0,2])rotate([103,0,0])cylinder(d=1,h=11);
    translate([0,0,2])rotate([-103,0,0])cylinder(d=1,h=11);
    cylinder(d=1,h=2);
    }
}