$fn=64;
$length=160;
$width=110;
$height=20;
$space=5;
difference(){
hull(){
translate([$space,$space,0])cylinder(h=$height-10,d=25);
translate([$length-$space,$space,0])cylinder(h=$height-10,d=25);
translate([$length-$space,$width-$space,0])cylinder(h=$height-10,d=25);
translate([$space,$width-$space,0])cylinder(h=$height-10,d=25);
    
    translate([$space,$space,$height-5])sphere(d=25);
translate([$length-$space,$space,$height-5])sphere(d=25);
translate([$length-$space,$width-$space,$height-5])sphere(d=25);
translate([$space,$width-$space,$height-5])sphere(d=25);
}
translate([5,5,-1])cube([50,100,32]);
translate([$length-50,55,-1])cylinder(h=32,d=90);
}
for(i = [8:10:$width]){   
translate([0,i,0])cube([$length,3,2]);
}
for(i = [8:10:$length]){   
translate([i,0,0])cube([3,$width,2]);
}
difference(){
translate([$length-50,55,0])cylinder(h=100,d=90);
    translate([$length-50,55,-1])cylinder(h=102,d=86);
}    