$fn=64;
$length=160;
$width=110;
$height=20;
$space=5;
include<text_extrude.scad>
module twist(){
linear_extrude(height = 100, twist = 180   , slices = 90) {
   difference() {
     offset(r = 30) {
      square(25, center = true);
     }
     offset(r = 27) {
       square(25, center = true);
     }
   }
 }
 }
 module twist_hull(){
linear_extrude(height = 100, twist = 180   , slices = 90) {
   difference() {
     offset(r = 30) {
      square(25, center = true);
     }
     offset(r = 0) {
       square(0, center = true);
     }
   }
 }
 }
difference(){
hull(){
translate([$space,$space,])cylinder(h=$height-10,d=25);
translate([$length-$space,$space,])cylinder(h=$height-10,d=25);
translate([$length-$space,$width-$space,])cylinder(h=$height-10,d=25);
translate([$space,$width-$space,])cylinder(h=$height-10,d=25);
    
    translate([$space,$space,$height-5])sphere(d=25);
translate([$length-$space,$space,$height-5])sphere(d=25);
translate([$length-$space,$width-$space,$height-5])sphere(d=25);
translate([$space,$width-$space,$height-5])sphere(d=25);
}
translate([40,-7.5,3]) rotate([90,0,0])text_extrude("Stefan E. L. E.");
translate([5,5,-1])cube([50,100,32]);
translate([110,55,0])twist_hull();
}
for(i = [8:10:$width]){   
translate([0,i,0])cube([$length,3,2]);
}
for(i = [8:10:$length]){   
translate([i,0,0])cube([3,$width,2]);
}
/*difference(){scale
translate([63,8,0])cube([94,94,100]);
    translate([65,10,-1])cube([90,90,104]);
}
*/
 translate([110,55,0])twist();