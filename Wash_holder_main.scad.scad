$fn=64;
$length=160;
$width=110;
$height=20;
$space=5;
difference(){
hull(){
translate([$space,$space,])cylinder(h=$height-10,d=25);
translate([$length-$space,$space,])cylinder(h=$height-10,d=25);
translate([$length-$space,$width-$space,])cylinder(h=$height-10,d=25);
translate([$space,$width-$space,])cylinder(h=$height-10,d=25);
    
    translate([$space,$space,$height-10])sphere(d=25);
translate([$length-$space,$space,$height-10])sphere(d=25);
translate([$length-$space,$width-$space,$height-10])sphere(d=25);
translate([$space,$width-$space,$height-10])sphere(d=25);
}
translate([5,5,-1])cube([50,100,32]);
translate([$length-95,10,-1])cube([90,90,32]);
}
for(i = [8:10:$width]){   
translate([0,i,0])cube([$length,3,2]);
}
for(i = [8:10:$length]){   
translate([i,0,0])cube([3,$width,2]);
}
difference(){
translate([63,8,0])cube([94,94,100]);
    translate([65,10,-1])cube([90,90,104]);
}    