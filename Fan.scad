/* Open SCAD Name.: CNC_Bit_Fan_v1.SCAD
*  Copyright (c)..: 2017 www.DIY3DTech.com
*
*  Creation Date..: 06/04/2017
*  Description....: CNC bit cooling and debris fan
*
*  Rev 1: Develop Model
*  Rev 2: 
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This code is supplied "as is" and high speed parts are dangerous
*  meaning the use of this is at YOUR OWN RISK!  
*
*/ 

/*------------------Customizer View-------------------*/

// preview[view:north, tilt:top]

/*---------------------Parameters---------------------*/

bit_dia         =   2.30;        //bit dia in mm

fan_dia         =   6 0.00;       //fan dia in mm
fan_height      =    5.00;       //height of fan

blade_count     =    5;          //blade count for fan 
blade_angle     =   -60;          //angle in degrees

nozzle_dia      =    0.4;        //nozzle dia in mm
shell_count     =    3;          //number of shell to print

fragment_count  =   100;         //number of fragments fn$

// Insert Bit from top of fan

/*-----------------------Execute----------------------*/

main_module();

/*-----------------------Modules----------------------*/

module main_module(){ //create module
    difference() {
            union() {//start union
                
             //create outer ring   
             ring_module();  
             //create inter-ring to connect to bit   
             translate ([0,0,0]) rotate ([0,0,0]) cylinder(fan_height,bit_dia+0.5,bit_dia+0.5,$fn=fragment_count,true); 
                
             //create blades  
        for (i=[0:(360/blade_count):360]) {
            //theta is degrees set by for loop from 0 to 360 (degrees)
            theta=i;
            //this sets the x axis point based on the COS of the theta
            x=0+(fan_dia/4)*cos(theta);
            //this sets the y axis point based on the sin of the theta
            y=0+(fan_dia/4)*sin(theta);
            //this creates the circle or other obect at the x,y point
            translate([x,y,0])rotate ([blade_angle,0,theta])cube([(fan_dia/2)-0.7,1.2,fan_height+3.5],true);
        }//end for loop for circle creation
            
 
                    } //end union
                            
    //start subtraction of difference
             //create tappered hole opening for bit       
             translate ([0,0,0]) rotate ([0,0,0]) cylinder(fan_height+0.5,(bit_dia/2),(bit_dia/2)+0.2,$fn=fragment_count,true); 
                    
              //diff out top portion of blades      
              translate([0,0,(fan_height+1.2)/2])rotate ([0,0,0])cube([(fan_dia*2),(fan_dia*2),1.2],true);
             //diff out bottom portion of blades      
             translate([0,0,-(fan_height+1.2)/2])rotate ([0,0,0])cube([(fan_dia*2),(fan_dia*2),1.2],true);
                                               
    } //end difference
}//end module

module ring_module(){ //create module
    difference() {
            union() {//start union
                
       translate ([0,0,0]) rotate ([0,0,0]) cylinder(fan_height,fan_dia/2,fan_dia/2,$fn=fragment_count,true);          
                        
                    } //end union
                            
    //start subtraction of difference
       translate ([0,0,0]) rotate ([0,0,0]) cylinder(fan_height+0.5,(fan_dia-(nozzle_dia*shell_count))/2,(fan_dia-(nozzle_dia*shell_count))/2,$fn=fragment_count,true);  
                                               
    } //end difference
}//end module


                 
/*----------------------End Code----------------------*/