$fn=64;
cube([15.8,15.8,3.2]);
translate([4,4,0])cylinder(d=5,h=1.7+3.2);
translate([12,4,0])cylinder(d=5,h=1.7+3.2);
translate([12,12,0])cylinder(d=5,h=1.7+3.2);
translate([4,12,0])cylinder(d=5,h=1.7+3.2);