rod="m3_rod.stl";
center=0;  // Center or by the side
hole=1; // Holes or cylinders?
scaling=1.2;
$fn=64;
distance_standart=10;
distance_adapter=18;
distance_standart_adapter=8; //only for "center=0"
if (center==0){
difference(){
    cube([10+distance_standart+distance_standart_adapter+distance_adapter,10,3]);
    translate([5,5,-1])cylinder(d=3.5,h=7);    
    translate([5+distance_standart,5,-1])cylinder(d=3.5,h=7);    
    if (hole==0){
    translate([5+distance_standart+distance_standart_adapter+distance_adapter,5,1])scale(scaling,scaling,0)import(rod);
    translate([5+distance_standart+distance_standart_adapter,5,1])scale(scaling,scaling,0)import(rod);
    }
    if (hole==1){
    translate([5+distance_standart+distance_standart_adapter+distance_adapter,5,-1])cylinder(d=3.5,h=7);    
    translate([5+distance_standart+distance_standart_adapter,5,-1])cylinder(d=3.5,h=7);    
    }
}
if (hole==0){
difference(){
    translate([5+distance_standart+distance_standart_adapter+distance_adapter,5,2])cylinder(d=5,h=5.5);
    translate([5+distance_standart+distance_standart_adapter+distance_adapter,5,1])scale(scaling,scaling,0)import(rod);
    translate([5+distance_standart+distance_standart_adapter+distance_adapter,5,1])cylinder(d1=3,d2=4,h=2);    
}
difference(){
    translate([5+distance_standart+distance_standart_adapter,5,2])cylinder(d=5,h=5.5);
    translate([5+distance_standart+distance_standart_adapter,5,1])scale(scaling,scaling,0)import(rod);
    translate([5+distance_standart+distance_standart_adapter,5,1])cylinder(d1=3,d2=4,h=2);    
}
}
}
if (center==1){
difference(){
    cube([10+distance_adapter,10,3]);
    translate([5+(distance_adapter-distance_standart)/2,5,-1])cylinder(d=3.5,h=7);    
    translate([5+(distance_adapter-distance_standart)/2+distance_standart,5,-1])cylinder(d=3.5,h=7);    
    if(hole==0){
    translate([5+distance_adapter,5,1])scale(scaling,scaling,0)import(rod);
    translate([5,5,1])scale(scaling,scaling,0)import(rod);
    }
    if(hole==1){
         translate([5+distance_adapter,5,-1])cylinder(d=3.5,h=7);
        translate([5,5,-1])cylinder(d=3.5,h=7);
}
}
if(hole==0){
difference(){
    translate([5+distance_adapter,5,2])cylinder(d=5,h=5.5);
    translate([5+distance_adapter,5,1])scale(scaling,scaling,0)import(rod);
    translate([5+distance_adapter,5,1])cylinder(d1=3,d2=4,h=2);    
}
difference(){
    translate([5,5,2])cylinder(d=5,h=5.5);
    translate([5,5,1])scale(scaling,scaling,0)import(rod);
    translate([5,5,1])cylinder(d1=3,d2=4,h=2);    
}
}
}