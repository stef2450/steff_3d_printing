$fn=100;
difference(){
union(){
translate([25, 0, 0])rotate_extrude(angle=180,convexity = 10)
translate([40, 0, 0])
circle(r = 3);
translate([-25, 0, 0])rotate_extrude(convexity = 10)
translate([40, 0, 0])
circle(r = 3);
}
translate([-25,-60,-10])cube([50,120,20]);
}
translate([-25,-40,0])rotate([0,90,0])cylinder(d=6,h=50);
translate([-25,40,0])rotate([0,90,0])cylinder(d=6,h=50);
for(i = [-30:10:30]){   
translate([i,-40,-3])cube([3,80,1]);
}
translate([-40,-37,-3])cube([3,74,1]);
translate([40,-37,-3])cube([3,74,1]);
for(i = [-11.5:10:10]){   
translate([-65,i,-3])cube([130,3,1]);
}
translate([-59,18.5,-3])cube([118,3,1]);
translate([-59,-21.5,-3])cube([118,3,1]);