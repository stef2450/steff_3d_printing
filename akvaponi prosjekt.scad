//Bottom shelf
    //Feet
cube([10,10,30]);
translate([0,40,0])cube([10,10,30]);
translate([90,40,0])cube([10,10,30]);
translate([90,0,0])cube([10,10,30]);
    //Plate
translate([0,0,30])cube([100,50,1.8]);
    //Support
translate([0,0,25.5]) cube([4.5,50,4.5]);
translate([95.5,0,25.5]) cube([4.5,50,4.5]);
translate([0,0,25.5]) cube([100,4.5,4.5]);
translate([0,45.5,25.5]) cube([100,4.5,4.5]);
//Top shelf
    //feet
translate([-4.5,-5,0])cube([4.5,9.5,120]);
translate([100,-5,0]) cube([4.5,9.5,120]);
translate([100,45.5,0]) cube([4.5,9.5,120]);
translate([-4.5,45.5,0]) cube([4.5,9.5,120]);
    //Plate
translate([-5,-5,120])cube([110,60,1.8]);    
    //support
translate([-4.5,0,116]) cube([4.5,50,4.5]);
translate([100,0,116]) cube([4.5,50,4.5]);
translate([0,50.5,116]) cube([100,4.5,4.5]);
translate([0,-5,116]) cube([100,4.5,4.5]);
//Aquarium
//translate([0,0,31.8])color([0.403, 0.996, 0.937])cube([100,50,50]);

