$fn = 100;
batteryDiameter = 18;
holderHeight = 15;
doubleHeight = 65+5;
holeSize= 1.5;
nBatteries=2;
module holder(){
union(){
difference() {
cylinder(h=holderHeight, d=batteryDiameter+2.5);
translate([0, 0, 0.9]) cylinder(h=holderHeight, d=batteryDiameter+0.5);
    cylinder(h=holderHeight, d=holeSize);
    translate([-batteryDiameter/2-4/2,-batteryDiameter/2-2.5,1.5])cube([batteryDiameter+4+5,7,holderHeight]);
    translate([-batteryDiameter/2-4/2,-batteryDiameter/2-8,0.9])cube([batteryDiameter+4+5,batteryDiameter+5.7,1]);
}
}
translate([0,0,0.7])union(){
difference(){
    translate([-3/2,-18/2,0])cube([3,18,2]);
    translate([-3/2,-24.5/2,0])cube([4,10.5,3]);
    rotate([0,0,180])translate([-3/2,-20.5/2,0])rotate([10,0,0])cube([4,10.5,3]);

    translate([0,1.5,1.3])rotate([-96,0,0])cylinder(d=1,h=11);
    cylinder(d=holeSize,h=2);
    }
}
}
module plate(){
difference(){    
translate([-(batteryDiameter+3)/2,batteryDiameter/2+0.7,0])cube([batteryDiameter+3.5,1.75,doubleHeight]);
for(j = [0:10:10]){ 
for(i = [0:10:60]){
        translate([j-5,13,i+5])rotate([90,0,0])cylinder(d=4.0,h=5);
    }
}
}
}
for(i = [0:20:(nBatteries-1)*20]){
difference(){
union(){
translate([i,0,0])plate();
translate([i,0,0])holder();
 translate([i,0,doubleHeight])mirror([0,0,1])holder();
}
for(i = [0:20:(nBatteries-2)*20]){
    for(j = [0:doubleHeight-1.5:doubleHeight]){ 
translate([i+batteryDiameter/2+0.5,-batteryDiameter/2,j])cube([1,batteryDiameter,2]);
 }
}
}
}