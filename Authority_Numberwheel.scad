dSize=60;
hSize=3;
textSize=4;
numbers=30;
difference(){
cylinder(d=dSize,h=hSize,$fn=12);
cylinder(d=8.6,h=hSize,$fn=64);
for(i=[0:numbers-1]){
rotate([0,0,(360/numbers)*i])translate([-dSize/2+5,0,-0.5])sphere(d=4,$fn=64);
    rotate([90,0,(360/numbers)*i+3])translate([(-dSize/2+5),0,-0.5])rotate([20,3,0])cylinder(d=2.5,h=4,$fn=64);
    //Få styr på den her efter, det ser grimt ud over med number > 20
    rotate([-90,0,(360/numbers)*i-3])translate([(-dSize/2+5),0,-0.5])rotate([20,3,0])cylinder(d=2.5,h=4,$fn=64);
}
}
translate([0,0,2])linear_extrude(height = 1.5) {
    for(i=[0:numbers-1]){
rotate([0,0,(360/numbers)*i])translate([-dSize/2+3,-textSize/2,2])text(str(i), font = "Liberation Sans:style=Bold", size = textSize);
    }
}
