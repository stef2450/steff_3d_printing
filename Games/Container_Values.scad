//General
LockSize=2;

//Circular
DiaMeter=50;
HeighT=30;
WallThikness=1;
BottomThikness=2;
LockNumber=4;

//Cube
CubeLength=110;
CubeWidth=60;
CubeHeight=30;
CubeWallthickness=1;
CubeBottomthickness=2;
CubeDiameter=(CubeLength+CubeWidth)/10;