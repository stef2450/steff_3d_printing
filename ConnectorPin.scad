module connectorPin(position) {
    difference(){
        translate(position)cylinder(d1=5,d2=3.4,h=5);
        translate(position)cube([1,5,10],center=true);
        difference(){
            translate(position + [0,0,0.7])cylinder(d=5,h=3.5);
            translate(position)cylinder(d=3,h=5);
        }
    }
}
