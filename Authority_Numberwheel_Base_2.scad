module Clicker(){
translate([0,0,0])cylinder(d=31,h=6,$fn=64);
for (i=[0:10-1]){
rotate([0,0,(360/10)*i])translate([0,-15.5,2.5])sphere(d=2,$fn=64);
}
}
difference(){
    union(){
translate([0,0,5])cube([38,44,10],center=true);
    translate([-19,0,22])rotate([0,90,0])cylinder(d=44,h=38);
    }
  translate([-20,0,22])rotate([0,90,0])cylinder(d=42,h=13);
    translate([7,0,22])rotate([0,90,0])cylinder(d=42,h=13);
translate([1,0,22])rotate([90,0,90])Clicker();
translate([-7,0,22])rotate([90,0,90])Clicker();
#translate([7,-7,40])cube([10,14,5]);
    #translate([-17,-7,40])cube([10,14,5]);
}
