$fn=10;
rotate([-10,0,0]) translate([0,-0.5,0]) cube([1,1,3]);
rotate([10,0,0]) translate([1,0.5,0]) cube([1,1,3]);
translate([0,0,3]) cube([2,1,3]);
translate([2,0,5]) cube([1,3,1]);
translate([-1,0,5]) cube([1,3,1]);
translate([0,-0.5,6]) cube([2,2,2]);
hull(){
    translate([-0,1.5,0])cylinder(d=2,h=0.2);
    translate([-0,-0.5,0]) cylinder(d=2,h=0.2);
    translate([2,1.5,0])cylinder(d=2,h=0.2);
    translate([2,-0.5,0])cylinder(d=2,h=0.2);
}