$fn=64;
$x=215;
$y=35;
difference(){
cube([230,50,4],center=true);
translate([-$x/2,-$y/2,-3])cylinder(d=6,h=6);
translate([$x/2,-$y/2,-3])cylinder(d=6,h=6);
translate([-$x/2,$y/2,-3])cylinder(d=6,h=6);
translate([$x/2,$y/2,-3])cylinder(d=6,h=6);
}
// control points of the handle rounded surface
q0 = [  [ [-50,0, 0],[-50,30, 0],[-150,0, 0],[-50,-30, 0],[-50,0, 0] ],
        [ [-50,0,25],[-50,15,25],[ -50,0,95],[-50,-15,25],[-50,0,25] ] ];
// add symetrical points
q = concat(q0, [ for(i=[1,0]) 
                  [for(pt=q0[i]) [ -pt.x, pt.y, pt.z ] ] ] );

show_CTP= false;

if(show_CTP)
    color("blue")
    for(p=q) polyhedron(p, [[for(i=[0:len(p)-1]) i]]);

render()
handle(q);

module handle(q, npu=60, npv=60) {
    // mesh of the handle rounded surface
    surf = Bezier_patch(q, degreeu=len(q)-1,degreev=len(q[0])-1,nu=npu,nv=npv);

    nu   = len(surf);
    nv   = len(surf[0]); 
    // the above surface in polyhedron format: inversion was needed
    poly_surf = mesh2polyhedron(surf, inv=true);
    // the two closing faces of the handle, extracted from the surface points
    face1 = surf[0];
    face2 = surf[nu-1];
    // a list of the above faces in polyhedron format: one face is inverted
    poly_faces = [ polygon2polyhedron(face1), 
                   polygon2polyhedron(face2,inv=true) ];
    // concatenating all in one list
    polys = concat([ poly_surf ], poly_faces );
    // the two center points of the handle holes: computed from surface points
    p1 = ( surf[0][nv/2] + surf[0][0] )/2;
    p2 = ( surf[nu-1][nv/2] + surf[nu-1][0] )/2;
    
    
        make_polyhedron(polys); // full solid handle
    }

// Bezier stuffs
function Bezier_patch(p, degreeu, degreev, nu, nv) =
    let( pu = [for(q=p) Bezier_curve(q,nv,degreev)] )
    Bezier_curve(pu,nu,degreeu);

function Bezier_curve(p, n=10, degree=3, from=0) =
    [ for(i=[0:n]) _uni_Bezier_eval(p, i/n, degree, from) ];

function _uni_Bezier_eval(p, u, degree=3, from=0) =
    _Casteljau([for(i=[0:degree]) p[from+i]], u);

function _Casteljau(p, u) =
    (len(p) == 2)?
        u*p[1] + (1-u)*p[0]:
        _Casteljau([for(i=[0:len(p)-2]) u*p[i+1] + (1-u)*p[i]], u);

// drawing module stuffs
function mesh2polyhedron(mesh, inv=false) =
    let( n = len(mesh) != 0 ? len(mesh) : 0,
         m = n==0 ? 0 : len(mesh[0]) != 0 ? len(mesh[0]) : 0 , 
         l = n*m)
    l > 0 ?
        let( range    = inv ? [len(mesh)-1: -1: 0] : [0:len(mesh)-1],
             vertices = l == 0 ? [] : [ for(i=range) for(pt=mesh[i]) pt ],    
             tris = concat(  [ for(i=[0:n-2],j=[0:m-2]) [ i*m+j, i*m+j+1, (i+1)*m+j ] ] ,
                         [ for(i=[0:n-2],j=[0:m-2]) [ i*m+j+1, (i+1)*m+j+1, (i+1)*m+j ] ] ) )
        [ vertices, tris ]:
        [] ;

function polygon2polyhedron(polygon, inv=false) =
     let( vertices  = polygon,
          range     = inv ? [len(polygon)-1: -1: 0] : [0:len(polygon)-1],
          facets    = [[for(i=range) i ]] )
    [ vertices, facets ];

module make_polyhedron(polys, convexity = 10) {
    function _accum_sum(l, offs=0, res=[]) =
        len(res) == len(l) ?
            res :
            _accum_sum(l, offs+l[len(res)], concat(res, [ offs+l[len(res)] ] ));

    function acc_len( f ) = 
        concat([0], _accum_sum([ for(fi=f) len(fi) ]));

    vertlist = [for(p=polys, pt=p[0]) pt]; // collect all verts from polyhedra
    vertlen  = [for(p=polys) p[0] ];
    acclen   = acc_len(vertlen);
    facets   = [ for(i=[0:len(polys)-1], f=polys[i][1] ) [ for(v=f) acclen[i]+v ] ];

    polyhedron(
        points = vertlist,
        faces  = facets,
        convexity = convexity
    );
}