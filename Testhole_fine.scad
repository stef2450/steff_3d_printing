rod="m3_rod.stl";
scaling=1.2;
$fn=64;

difference(){
cube([10,10,3]);
    translate([5,5,1])scale(scaling,scaling,0)import(rod);
}
difference(){
translate([5,5,2])cylinder(d=5,h=5.5);
translate([5,5,1])scale(scaling,scaling,0)import(rod);
translate([5,5,6.5])cylinder(d1=3,d2=4,h=2);    
}