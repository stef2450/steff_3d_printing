$fn=64;
point=0.00001;
difference(){
translate([0,0,0.1])rotate([0,90,0]) cylinder(d=3.8,h=85);
translate([80,0,-2.5]) cube(5);
}
hull(){
translate([75/2,0,0])cube([10,6.8,3.6],center=true);
    translate([75/2,0,-2.4+6])cube([10,point,point],center=true);
}
