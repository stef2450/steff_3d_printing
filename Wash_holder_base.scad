$fn=64;
$length=160;
$width=110;
$height=15;
$space=3;
difference(){
hull(){
cylinder(h=$height,d=25,center=true);
translate([$length,0,0])cylinder(h=$height,d=25,center=true);
translate([$length,$width,0])cylinder(h=$height,d=25,center=true);
translate([0,$width,0])cylinder(h=$height,d=25,center=true);
}
hull(){
translate([$space,$space,$space])cylinder(h=$height,d=25,center=true);
translate([$length-$space,$space,$space])cylinder(h=$height,d=25,center=true);
translate([$length-$space,$width-$space,$space])cylinder(h=$height,d=25,center=true);
translate([$space,$width-$space,$space])cylinder(h=$height,d=25,center=true);
}
}
translate([$length/2,-7,-3.5])sphere(d=8,center=true);
translate([$length/2,$width+8,-3.5])sphere(d=8,center=true);
translate([-7 ,$width/2,-3.5])sphere(d=8,center=true);
translate([$length+8 ,$width/2,-3.5])sphere(d=8,center=true);