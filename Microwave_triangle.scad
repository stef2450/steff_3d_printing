$fn = 100;
$R=12;
difference () {
  translate([0,0,0]) cylinder(d=10,h=17);
  difference(){
    translate([0,0,-1]) cylinder(d=6.5,h=16);
        translate([-5,2.8,-1]) cube([10,10,17]);    
    }
}
hull(){
    translate([sin(0)*$R,cos(0)*$R,16.5]) cylinder(d=12,h=2);
    translate([sin(120)*$R,cos(120)*$R,16.5]) cylinder(d=12,h=2);
    translate([sin(-120)*$R,cos(-120)*$R,16.5]) cylinder(d=12,h=2);
}